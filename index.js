// Setup basic express server
var express = require('express');
var redis = require('redis');
var redisClient = redis.createClient();
var app = express();
var path = require('path');
var server = require('http').createServer(app);
const io = require('socket.io')(server);
var port = process.env.PORT || 8080;

// Routing
app.use(express.static(path.join(__dirname, 'public')));

// Chatroom
var message = [];
var storeMessages = function(name,data){
  var message = JSON.stringify({name:name,data,data});
  redisClient.lpush("messages",message,function(err,response){
    redisClient.ltrim("messages",0 ,9);
  })
}
io.on('connection', (client) => {
  client.on('join',function(name){
    client.broadcast.emit("add chatter",name);
    redisClient.smembers('name',function(error,names){
      names.forEach(function(name){
        client.emit('add chatter',name);
      });
    });
    redisClient.sadd('chatter',name);
    redisClient.lrange("messages",0,-1,function(err,message){
      message = message.reverse();
      message.forEach(function(message){
        message = JSON.parse(message);
        client.emit("messages",message.name +":" + message.data);
      })
      client.nickname = name;
    });
  });
  client.on('messages',(data) => {
    let nickname = client.nickname;
    client.broadcast.emit('messages',nickname + ":" + data);
    client.emit("messages", nickname +":" + data);
    storeMessages(nickname,data);
  });
  client.on('disconnect',function(name){
    redisClient.get('nickname',function(error,name){
      client.broadcast.emit('remove chatter',name);
      redisClient.srem('chatters',name);
    })
  });
});


server.listen(port, () => {
  console.log('Server listening at port %d', port);
});