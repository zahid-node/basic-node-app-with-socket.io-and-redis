$(function() {
    var socket = io();
    $('#chat-form').submit(function(e){
        var message = $('#chat_input').val();
        socket.emit('messages',message);
    });
});